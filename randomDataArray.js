var { getRandomInt } = require('./utils')

function randomArr(length, amount) {
    var delimiter = " "
    var arr = ["Wulan", "Raharjo", "Widya", "Yuda", "Cinta", "Iskandar", "Hidayat", "Kusuma", "Indah", "Jusuf"]
    var result = []

    while (length > 0) {
        var temp = ""
        for(var i = 0; i < amount; i++) {
            let data = arr[getRandomInt(0, arr.length)]
            temp += data + delimiter
        }
        temp = temp.substr(0, temp.length-1)
        result.push(temp)
        length--
    }
    
    return result
}

module.exports = {
    randomArr
}