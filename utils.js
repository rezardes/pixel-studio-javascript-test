function generateRandomChar() {
    let code = getRandomInt(48, 123)
    var char = String.fromCharCode(code)
    return char
}

function generateRandomString(length) {
    var result = ""
    for(var i = 0; i < length; i++) {
        result += generateRandomChar()
    }
    return result
}

function reverseString(str) {

    let splits = str.split("");
    let reverse = splits.reverse();
    var result = reverse.join("");
    
    return result;
}

function getRandomInt(max, min) {
    return (Math.floor(Math.random() * (max - min)) + min)
}

module.exports = {
    generateRandomChar,
    generateRandomString,
    reverseString,
    getRandomInt
}

