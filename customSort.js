function customSort(arr, asc = true) {
    var compare = (asc) ? (elmt1, elmt2) => elmt1 > elmt2
                        : (elmt1, elmt2) => elmt1 < elmt2
    for(var i=0;i<arr.length;i++) {
        for(var j=i+1;j<arr.length;j++) {
            if (compare(arr[i], arr[j])) {
                temp = arr[i]
                arr[i] = arr[j]
                arr[j] = temp
            }
        }
    }

    return arr
}

module.exports = {
    customSort
}