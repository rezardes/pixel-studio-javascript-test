var {randomArr} = require('./randomDataArray')
var {customSort} = require('./customSort')
var {generateRandomPalindrome} = require('./randomPalindrome')

console.log(randomArr)

console.log('randomDataArray')
console.log(randomArr(1, 2))
console.log(randomArr(1, 1))
console.log(randomArr(3, 3))

console.log('\ncustomSort')
console.log(customSort(['makan', 'sosis', '123', 'pasta']))
console.log(customSort([2.7, 1.5, 6.4, 8.0], false))
console.log(customSort([-1, 7, -6]))
console.log(customSort(['A', 'Z', 'V'], false))

console.log('\nrandomPalindrome')
console.log(generateRandomPalindrome(1))
console.log(generateRandomPalindrome(2))
console.log(generateRandomPalindrome(5))
console.log(generateRandomPalindrome(6))