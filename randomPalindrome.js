var {generateRandomChar, generateRandomString, reverseString } = require('./utils')

function generateRandomPalindrome(length) {
    let side = Math.floor(length/2)
    side = (side === 0) ? 1 : side
    let leftSide = generateRandomString(side)
    let middle = (length % 2 == 1 && length > 1) ? generateRandomChar() : "" 
    let rightSide = (length > 1) ? reverseString(leftSide) : ""

    var result = leftSide + middle + rightSide
    return result
}

module.exports = {
    generateRandomPalindrome
}